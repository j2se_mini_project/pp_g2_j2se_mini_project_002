package com.company;

import java.sql.*;
import java.util.ArrayList;

public class Query {

    public void setDisplayRow(Connection connection, int row) throws SQLException {
        try {
            PreparedStatement ps = connection.prepareStatement("UPDATE tbl_setting SET row = ? WHERE id = 1");
            ps.setInt(1, row);
            int insert = ps.executeUpdate();
            System.out.println("*** Result : " + insert + " Row Insert ***");
            ps.close();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    public void insert(Connection connection, Product p, String table) {
        try {
            PreparedStatement ps = connection.prepareStatement("INSERT INTO " + table + " VALUES (?, ?, ?, ?, ?)");
            ps.setInt(1, p.getId());
            ps.setString(2, p.getName());
            ps.setDouble(3, p.getUnitPrice());
            ps.setInt(4, p.getQty());
            ps.setDate(5, Date.valueOf(p.getImportedDate()));
            int insert = ps.executeUpdate();
            System.out.println("*** Result : " + insert + " Row Insert ***");
            ps.close();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    public void delete(Connection connection, int id) {
        try {
            PreparedStatement ps = connection.prepareStatement("DELETE FROM tbl_temp WHERE product_id = ?");
            ps.setInt(1, id);
            int row = ps.executeUpdate();
            System.out.println("**** Result : " + row + " Row(s) ****");
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateAll(Connection connection, Product p) {
        try {
            PreparedStatement ps = connection.prepareStatement("UPDATE tbl_temp SET product_name = ?, unit_price = ?, qty = ? WHERE product_id = ?");
            ps.setString(1, p.getName().trim());
            ps.setDouble(2, p.getUnitPrice());
            ps.setInt(3, p.getQty());
            ps.setInt(4, p.getId());
            int row = ps.executeUpdate();
            System.out.println("**** Result : " + row + " Row(s) ****");
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateByName(Connection connection, Product p) {
        try {
            PreparedStatement ps = connection.prepareStatement("UPDATE tbl_temp SET product_name = ? WHERE product_id = ?");
            ps.setString(1, p.getName().trim());
            ps.setInt(2, p.getId());
            int row = ps.executeUpdate();
            System.out.println("**** Result : " + row + " Row(s) ****");
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateByPrice(Connection connection, Product p) {
        try {
            PreparedStatement ps = connection.prepareStatement("UPDATE tbl_temp SET unit_price = ? WHERE product_id = ?");
            ps.setDouble(1, p.getUnitPrice());
            ps.setInt(2, p.getId());
            int row = ps.executeUpdate();
            System.out.println("**** Result : " + row + " Row(s) ****");
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateByQty(Connection connection, Product p) {
        try {
            PreparedStatement ps = connection.prepareStatement("UPDATE tbl_temp SET qty = ? WHERE product_id = ?");
            ps.setInt(1, p.getQty());
            ps.setInt(2, p.getId());
            int row = ps.executeUpdate();
            System.out.println("**** Result : " + row + " Row(s) ****");
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void saveData(Connection connection) {
        addExistingRow(connection, "tbl_products", "tbl_temp");
        updateExistingRow(connection);
    }

    public void addExistingRow(Connection connection, String receiveTable, String givenTable ){
        try {
            String str = "INSERT INTO " + receiveTable + " " +
                    "SELECT * FROM " + givenTable + " WHERE NOT EXISTS( " +
                    "SELECT * FROM " + receiveTable + " " +
                    "WHERE "+ receiveTable +".product_id = "+ givenTable +".product_id);";
            PreparedStatement ps = connection.prepareStatement(str);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateExistingRow(Connection connection){
        try{
            String str = "UPDATE tbl_products AS prod " +
                    "SET product_name = temp.product_name, unit_price = temp.unit_price, qty = temp.qty " +
                    "FROM tbl_temp as temp " +
                    "WHERE prod.product_id = temp.product_id;";
            PreparedStatement ps = connection.prepareStatement(str);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public int selectMaxID(Connection connection, String table) {
        int id = 0;
        try {
            String query = "SELECT MAX(product_id) FROM " + table;
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                ResultSet r = resultSet;
                id = r.getInt(1);
            }
            resultSet.close();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NullPointerException e){
            id = 0;
        }
        return id;
    }

    public int getDisplayRow(Connection connection){
        int row = 0;
        try {
            String query = "SELECT row FROM tbl_setting;";
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                ResultSet r = resultSet;
                row = r.getInt(1);
            }
            resultSet.close();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NullPointerException e){
            row = 0;
        }
        return row;
    }

    public boolean checkTheSameTable(Connection connection){
        boolean checkTrue = false;
        try{
            String query = "SELECT * " +
                    "FROM tbl_temp " +
                    "LEFT JOIN tbl_products ON (tbl_temp.product_name like tbl_products.product_name) " +
                    "WHERE tbl_products.product_id IS NULL OR tbl_products.product_name <> tbl_temp.product_name " +
                    "OR tbl_products.qty <> tbl_temp.qty OR tbl_products.unit_price <> tbl_temp.unit_price";
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet resultSet = ps.executeQuery();
            int count = 0;
            while (resultSet.next()) {
                ResultSet r = resultSet;
                count ++;
            }
            if(count > 0) checkTrue = false;
            else if (countRow(connection, "tbl_temp") != countRow(connection, "tbl_products")) checkTrue = false;
            else checkTrue = true;
            resultSet.close();
            ps.close();
        } catch (SQLException e){
            e.printStackTrace();
        }
        return checkTrue;
    }

    public void addRowToList(Connection connection, ArrayList<Product> arr, String table){
        try {
            String query = "SELECT * FROM " + table + " ORDER BY product_id ASC";
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                ResultSet r = resultSet;
                Product p = new Product(r.getInt(1), r.getString(2), r.getDouble(3), r.getInt(4), r.getDate(5) + "");
                arr.add(p);
            }
            resultSet.close();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAll(Connection connection, String table){
        try{
            String str = "DELETE FROM " + table;
            PreparedStatement ps = connection.prepareStatement(str);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public int countRow(Connection connection, String table){
        int count = 0;
        try {
            String query = "SELECT COUNT(*) AS row_count FROM " + table;
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                ResultSet r = resultSet;
                count = r.getInt("row_count");
            }
            resultSet.close();
            ps.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return count;
    }
}
