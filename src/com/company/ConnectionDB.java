package com.company;

import java.sql.*;
import java.util.Properties;

public class ConnectionDB {
    public Connection getConnectionDB() {
        Connection connection = null;
        try {
            Class.forName("org.postgresql.Driver");
            Properties info = new Properties();
            info.put("user", "santarak");
            info.put("password", "incorrect");
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/product_db", info);
        } catch (ClassNotFoundException e){
            e.printStackTrace();
        } catch (SQLException e){
            e.printStackTrace();
        }
              return connection;
    }
}
