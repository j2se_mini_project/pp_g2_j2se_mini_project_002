package com.company;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) throws IOException, SQLException, ClassNotFoundException {

        Scanner scanner = new Scanner(System.in);
        Method method = new Method();

        method.PrintConsole();

        while (true) {
            method.menu();

            System.out.print("command --> ");
            String command = scanner.nextLine();

            String regex10m = "[_]{1}[+]?\\d*\\.?\\d+[Mm]{1}";
            String wShortCutRegex = "[Ww]{1}[#]{1}[a-z A-Z]+[-]{1}[+]?\\d*\\.?\\d+[-]{1}[0-9]+";
            String rShortCutRegex = "[Rr]{1}[#]{1}[0-9]+";
            String dShortCutRegex = "[Dd]{1}[#]{1}[0-9]+";
            String str = command;
            if(command.matches(regex10m)) command = "_10M";
            else if(command.matches(wShortCutRegex)) command = "w#null-null-null";
            else if(command.matches(rShortCutRegex)) command = "r#null";
            else if(command.matches(dShortCutRegex)) command = "d#null";

            int num = 0;
            double _10M = 0;
            Pattern p;
            Matcher m;

            switch (command.toLowerCase().trim()) {
                case "*":
                    method.displayProduct(method.getProducts());
                    break;
                case "w":
                    method.insertProduct(method.getProducts());
                    break;
                case "r":
                    method.viewProducts(method.getProducts());
                    break;
                case "d":
                    method.deleteProduct(method.getProducts());
                    break;
                case "u":
                    method.updateProduct(method.getProducts());
                    break;
                case "f":
                    method.paginationFirst(method.getProducts());
                    break;
                case "p":
                    method.paginationPrevious(method.getProducts());
                    break;
                case "n":
                    method.paginationNext(method.getProducts());
                    break;
                case "l":
                    method.paginationLast(method.getProducts());
                    break;
                case "s":
                    method.searchByProductName(method.getProducts());
                    break;
                case "g":
                    method.gotoPage(method.getProducts());
                    break;
                case "se":
                    method.setDisplayRow();
                    break;
                case "sa":
                    method.save();
                    break;
                case "ba":
                    method.backupData();
                    break;
                case "re":
                    method.restore();
                    break;
                case "h":
                    method.help();
                    break;
                case "_10m":
                    p = Pattern.compile("\\d+");
                    m = p.matcher(str);
                    while (m.find()) {
                        _10M = Double.parseDouble(m.group());
                    }
                    method.add10MRecord(method.getProducts(), _10M);
                    break;
                case "w#null-null-null":
                    str = str.replace("#", "-");
                    String[] segments = str.split("-");
                    String pName = segments[segments.length - 3];
                    double price = Double.parseDouble(segments[segments.length - 2]);
                    int qty = Integer.parseInt(segments[segments.length - 1]);
                    method.insertProductByShortcut(method.getProducts(),pName, price, qty);
                    break;
                case "r#null":
                    p = Pattern.compile("\\d+");
                    m = p.matcher(str);
                    while (m.find()) num = Integer.parseInt(m.group());
                    method.viewProductByShortcut(method.getProducts(), num);
                    break;
                case "d#null":
                    p = Pattern.compile("\\d+");
                    m = p.matcher(str);
                    while (m.find()) num = Integer.parseInt(m.group());
                    method.deleteProductByShortcut(method.getProducts(), num);
                    break;
                case "e":
                    System.exit(0);
                    break;
                default:
                    method.warningTable("WARNING : COMMAND \"" + command + "\" Not Found. Please Input Again. Thanks you !");
                    method.help();
            }
        }
    }
}